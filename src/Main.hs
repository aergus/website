{-
 - Main.hs
 -
 - Written in 2014-2021 by Aras Ergus <arasergus@posteo.net>
 -
 - To the extent possible under law, the author(s) have dedicated all copyright
 - and related and neighboring rights to this software to the public domain
 - worldwide by associating CC0 with it. This software is distributed without
 - any warranty.
 -
 - For more information, please see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

{-# LANGUAGE OverloadedStrings #-}

--------------------------------------------------------------------------------

import           KaTeXWrapper (pandocCompilerWithKaTeX)

import           Data.Monoid (Any(Any, getAny))
import           Data.String (fromString)
import           Hakyll ( Compiler
                        , Context
                        , Item
                        , (.||.)
                        , applyAsTemplate
                        , boolField
                        , compile
                        , compressCssCompiler
                        , constField
                        , copyFileCompiler
                        , customRoute
                        , defaultContext
                        , getResourceBody
                        , getResourceFilePath
                        , hakyll
                        , idRoute
                        , itemBody
                        , listField
                        , loadAll
                        , loadAndApplyTemplate
                        , match
                        , pandocCompiler
                        , readPandoc
                        , renderPandoc
                        , recentFirst
                        , relativizeUrls
                        , route
                        , saveSnapshot
                        , setExtension
                        , templateCompiler
                        , toFilePath
                        )
import           Hakyll.FileStore.Git.Context (gitModificationTimeField)
import           System.FilePath ( joinPath
                                 , splitPath
                                 , takeBaseName
                                 )
import           Text.Pandoc (Inline (Math))
import           Text.Pandoc.Walk (query)

--------------------------------------------------------------------------------

main :: IO ()
main = hakyll $ do
    match "root/**" $ do
        route (customRoute $ joinPath . drop 1 . splitPath . toFilePath)
        compile copyFileCompiler

    match ("academic/documents/*/**" .||.
           "notes/**"                .||.
           "files/**"                .||.
           "css/fonts/*"             .||.
           "js/*") $ do
        route   idRoute
        compile copyFileCompiler

    match "css/*.css" $ do
        route   idRoute
        compile compressCssCompiler

    match "academic/texts/*" $
        -- Do some dummy rendering in order to collect metadata.
        compile $ pandocCompiler >>= saveSnapshot "academic-texts"

    match "404.md" $ do
        route   $ setExtension "html"
        compile $ pandocCompiler
            >>= useTemplateByName "page"    defaultContext
            >>= useTemplateByName "default" defaultContext
            -- CAUTION: Omitting this requires absolute paths to be correct,
            -- but it should be OK for a 404 page.
            -- >>= relativizeUrls

    match "academic/index.md" $ do
        route $ setExtension "html"
        compile $ do
            texts <- recentFirst =<< loadAll "academic/texts/*"
            let textsCtx =
                    listField "texts" acTextCtx (return texts) <>
                    defaultContext
            getResourceBody
                >>= applyAsTemplate textsCtx
                >>= renderPandoc
                >>= useTemplateByName "page"    defaultCtx
                >>= useTemplateByName "default" (highlightCtx "academic")
                >>= relativizeUrls

    match "academic/documents/index.md" $ do
        route   $ setExtension "html"
        compile $ do
            mathCtx <- getMathCtx
            let docsCtx =
                    mathCtx                 <>
                    highlightCtx "academic" <>
                    highlightCtx "academic-documents"
            pandocCompilerWithKaTeX
                >>= useTemplateByName "page"    defaultCtx
                >>= useTemplateByName "default" docsCtx

    match "index.md" $ do
        route   $ setExtension "html"
        compile $ do
            pandocCompiler
                >>= useTemplateByName "page"    defaultCtx
                >>= useTemplateByName "default" (highlightCtx "home")
                >>= relativizeUrls

    match "about-this-site.md" $ do
        route   $ setExtension "html"
        compile $ do
            highlight <- fmap takeBaseName getResourceFilePath
            pandocCompiler
                >>= useTemplateByName "page"    defaultCtx
                >>= useTemplateByName "default" (highlightCtx highlight)
                >>= relativizeUrls

    match (fromString (templatePath "*")) $ compile templateCompiler

-------------------------------------------------------------------------------

getMathCtx :: Compiler (Context String)
getMathCtx = do
    pandocItem <- readPandoc =<< getResourceBody
    return $ boolField "render-math" $ const $ getAny $
        query extractMathInfo $ itemBody pandocItem
    where extractMathInfo (Math _ _) = Any True
          extractMathInfo _          = Any False

--------------------------------------------------------------------------------

defaultCtx :: Context String
defaultCtx = gitModificationTimeField "modification-time" "%H:%M"     <>
             gitModificationTimeField "modification-date" "%B %e, %Y" <>
             defaultContext

highlightCtx :: String -> Context String
highlightCtx s = (constField ("highlight-" ++ s) "") <> defaultContext

acTextCtx :: Context String
acTextCtx = defaultCtx <> highlightCtx "academic"

--------------------------------------------------------------------------------

templatePath :: String -> String
templatePath s = "templates/" ++ s ++ ".html"

useTemplateByName :: String -> Context a -> Item a -> Compiler (Item String)
useTemplateByName =
    loadAndApplyTemplate . fromString . templatePath
