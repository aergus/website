{-
 - KaTeXWrapper.hs
 -
 - Written in 2016-2024 by Aras Ergus <arasergus@posteo.net>
 -
 - To the extent possible under law, the author(s) have dedicated all copyright
 - and related and neighboring rights to this software to the public domain
 - worldwide by associating CC0 with it. This software is distributed without
 - any warranty.
 -
 - For more information, please see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

{-# LANGUAGE OverloadedStrings #-}

--------------------------------------------------------------------------------

module KaTeXWrapper (pandocCompilerWithKaTeX) where

--------------------------------------------------------------------------------

import           Control.Monad (liftM, unless)
import qualified Control.Monad.Writer.Lazy as W (Writer, writer, execWriter)
import           Data.Aeson (eitherDecode, encode)
import qualified Data.Aeson.KeyMap as KM (toList)
import qualified Data.Aeson.Types as AT ( FromJSON
                                        , Value (Object)
                                        , parseJSON
                                        , typeMismatch
                                        )
import qualified Data.ByteString.Lazy.Char8 as BS ( hGetContents
                                                  , hPut
                                                  , hPutStrLn
                                                  , null
                                                  , pack
                                                  )
import           Data.Hashable (Hashable, hashWithSalt)
import qualified Data.HashMap.Lazy as HM ( HashMap
                                         , (!)
                                         , fromList
                                         , toList
                                         )
import qualified Data.Set as S (Set, singleton, toList)
import           Data.Text (Text)
import           Hakyll ( Compiler
                        , Item
                        , defaultHakyllReaderOptions
                        , defaultHakyllWriterOptions
                        , pandocCompilerWithTransformM
                        , unsafeCompiler
                        )
import           System.Exit (ExitCode(ExitSuccess, ExitFailure))
import           System.IO (hSetBinaryMode, hClose, stderr)
import           System.Process ( StdStream (CreatePipe)
                                , createProcess
                                , shell
                                , std_err
                                , std_in
                                , std_out
                                , waitForProcess
                                )
import           Text.Pandoc ( Format (Format)
                             , Inline (Math, RawInline)
                             , MathType (DisplayMath, InlineMath)
                             , Pandoc
                             )
import           Text.Pandoc.Walk (walk, walkM)

--------------------------------------------------------------------------------

newtype HashableMathType = MT MathType deriving (Eq, Ord, Show)

instance Hashable HashableMathType where
  hashWithSalt n (MT DisplayMath) = n
  hashWithSalt n (MT InlineMath)  = n + 1

newtype MathMap =
    MM (HM.HashMap HashableMathType (HM.HashMap Text Text)) deriving (Eq, Show)

instance AT.FromJSON MathMap where
    parseJSON (AT.Object v) = liftM MM $ do
       list <- mapM
           (\ (k, v') -> case k of
                             "DisplayMath" -> do tm <- AT.parseJSON v'
                                                 return (MT DisplayMath, tm)
                             "InlineMath"  -> do tm <- AT.parseJSON v'
                                                 return (MT InlineMath, tm)
                             _             -> AT.typeMismatch
                                 "HMap.HashMap Text Text @ MathMap" v')
           (KM.toList v)
       return $ HM.fromList list
    parseJSON v            = AT.typeMismatch "MathMap" v

--------------------------------------------------------------------------------

scriptPath :: String
scriptPath = "../scripts/run-math-jobs-with-katex.js"

nodejs :: String
nodejs = "nodejs"

command :: String
command = nodejs ++ " " ++ scriptPath

--------------------------------------------------------------------------------

pandocCompilerWithKaTeX :: Compiler (Item String)
pandocCompilerWithKaTeX = pandocCompilerWithTransformM
    defaultHakyllReaderOptions defaultHakyllWriterOptions transformMath

transformMath :: Pandoc -> Compiler Pandoc
transformMath p = unsafeCompiler $ do
    let jobs = W.execWriter $ mathJobs p
    if null $ S.toList jobs
    then return p
    else do
        m <- runMathJobs jobs
        return $ walk (applyMathInline m) p

applyMathInline :: MathMap ->  Inline -> Inline
applyMathInline (MM m) (Math mathType tex) =
    RawInline (Format "html") $ (m HM.! MT mathType) HM.! tex
applyMathInline _      x                   = x

mathJobs :: Pandoc -> W.Writer (S.Set (MathType, Text)) Pandoc
mathJobs = walkM mathJob

mathJob :: Inline -> W.Writer (S.Set (MathType, Text)) Inline
mathJob x@(Math mathType tex) = W.writer (x, S.singleton (mathType, tex))
mathJob x                     = return x

--------------------------------------------------------------------------------

runMathJobs ::
    S.Set (MathType, Text) -> IO MathMap
runMathJobs s = do
    let cp = (shell command)
                 { std_in  = CreatePipe
                 , std_out = CreatePipe
                 , std_err = CreatePipe
                 }
    (Just inputH, Just outputH, Just errorH, process) <- createProcess cp
    hSetBinaryMode inputH  True
    hSetBinaryMode outputH True
    BS.hPut inputH $ encode $ S.toList s
    hClose inputH
    out <- BS.hGetContents outputH
    err <- BS.hGetContents errorH
    exit <- waitForProcess process
    unless (BS.null err) $ do
        errPutStrLn "\nProblem while rendering LaTeX code:"
        errPutStrLn err
    putStrLn $ indent ("run a batch of " ++ show (length s) ++
                       " math rendering job(s) with " ++ prettyExitCode exit)
    let result = eitherDecode out
    case result of
        Right v -> return v
        Left m  -> do
            errPutStrLn "\nProblem while parsing rendered LaTeX code:"
            errPutStrLn $ BS.pack m
            let (dm, im) = rawMathCodes (S.toList s) (mempty, mempty)
            return $ MM $ HM.fromList [ (MT DisplayMath, dm)
                                      , (MT InlineMath, im)
                                      ]
    where errPutStrLn = BS.hPutStrLn stderr
          prettyExitCode ExitSuccess     = "success"
          prettyExitCode (ExitFailure n) = "exit code " ++ show n
          rawMathCodes []                        (dm, im) =
              (dm, im)
          rawMathCodes ((DisplayMath, tex) : xs) (dm, im) =
              rawMathCodes xs
                           ( dm `mappend` HM.fromList [(tex, tex)]
                           , im
                           )
          rawMathCodes ((InlineMath, tex) : xs)  (dm, im) =
              rawMathCodes xs
                           ( dm
                           , im `mappend` HM.fromList [(tex, tex)]
                           )

--------------------------------------------------------------------------------

indent :: String -> String
indent = ("  " ++) . indent'
    where indent' []          = []
          indent' ['\n']      = ['\n']
          indent' ('\n' : cs) = "\n  " ++ indent' cs
          indent' (c : cs)    = c : indent' cs
