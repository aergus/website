TARGET = site

SRCDIR = src

KATEXDIR = _katex
KATEXVERSION = 0.16.9
KATEXFILE = katex.tar.gz
BASEURL =  https://github.com/Khan/KaTeX/releases/download
KATEXURL = $(BASEURL)/v$(KATEXVERSION)/$(KATEXFILE)
HASHBEGIN = 61a8a1f5d6b17e91c1c7dda077a480952c41b76027227a57739d6b0712f199a0
HASHEND   = 6bf693f512eaeb68075516baa8e3eaca8211e021e35a4f33547c7fc7c4ae87b7
KATEXHASH = $(HASHBEGIN)$(HASHEND)

JSDIR = scripts

RELTEMPLATEPATH = ../templates
CSSDIR = css
RELCSSPATH = ../$(CSSDIR)

CONTENTDIR = content
CONTENTREPOURL = https://gitlab.com/aergus/website.git
CONTENTBRANCH = content

default: build

clone-content:
	if [ ! -d $(CONTENTDIR) ]; then \
          echo "\n*** Couldn't found the directory '$(CONTENTDIR)'," \
               "cloning anew... ***\n"; \
          git clone --recursive -b \
            $(CONTENTBRANCH) $(CONTENTREPOURL) $(CONTENTDIR); \
        else \
          echo "\n*** The directory '$(CONTENTDIR)' already exists. ***\n"; \
        fi

create-links: clone-content
	rm -f $(CONTENTDIR)/templates
	cd $(CONTENTDIR) && ln -s $(RELTEMPLATEPATH) templates
	rm -f $(CONTENTDIR)/css
	cd $(CONTENTDIR) && ln -s $(RELCSSPATH) css
	echo "\n*** Links (re)created. ***\n"

prepare-katex: clone-content
	if [ ! -d $(KATEXDIR) ] || [ ! -e $(KATEXDIR)/$(KATEXFILE) ]; then \
          echo "\n*** Couldn't find KaTeX, downloading it... ***\n"; \
          mkdir -p $(KATEXDIR) && \
          cd $(KATEXDIR) && wget -c $(KATEXURL); \
        fi
	cd $(KATEXDIR) && \
        if [ "`sha512sum $(KATEXFILE) | cut -d ' ' -f 1`" = \
               "$(KATEXHASH)" ]; then \
          tar -xvf $(KATEXFILE) && \
          cp -a katex/fonts $(RELCSSPATH)/fonts && \
          cp katex/katex.min.css $(RELCSSPATH)/katex.min.css; \
          cp katex/katex.min.js ../$(JSDIR)/katex.min.js; \
          echo "\n*** KaTeX is deployed. ***\n"; \
        else \
          echo "\n!!! ERROR: Hashes for '$(KATEXFILE)' do not match!" \
               "Please remove it and run this rule again. !!!\n" && \
          exit 1; \
        fi

compile:
	cabal install --installdir=.
	echo "\n*** Binary compilation completed. ***\n"

build: compile create-links prepare-katex
	cd $(CONTENTDIR) && ../$(TARGET) build

clean-site:
	rm -rf $(CONTENTDIR)/_site
	rm -rf $(CONTENTDIR)/_cache
	rm -rf $(CONTENTDIR)/css
	rm -rf $(CONTENTDIR)/templates

clean-katex:
	rm -rf $(KATEXDIR)
	rm -rf $(CSSDIR)/fonts
	rm -f $(CSSDIR)/katex.min.css
	rm -f $(JSDIR)/katex.min.js

clean-compilation:
	cabal clean
	rm -rf dist-newstyle

clean: clean-site clean-compilation

rebuild-site: compile create-links prepare-katex
	cd $(CONTENTDIR) && ../$(TARGET) rebuild

recompile: clean-compilation compile

rebuild: clean build

watch-site: compile create-links prepare-katex
	cd $(CONTENTDIR) && ../$(TARGET) watch

watch: compile watch-site

rewatch: rebuild watch-site

rewatch-site: rebuild-site watch-site

.PHONY = clean clean-site clean-compilation clean-katex create-links
