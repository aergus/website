/*
 * run-math-jobs-with-katex.hs
 *
 * Written in 2016 by Aras Ergus <arasergus@posteo.net>
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide by associating CC0 with it. This software is distributed without
 * any warranty.
 *
 * For more information, please see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */


/* May the gods of purity forgive me for mixing JavaScript with Haskell... */

var name = 'run-math-jobs-with-katex'

var katex = require('./katex.min.js')

var exitCode = 0;

var jobsString = '';
process.stdin.setEncoding('utf-8');
process.stdin.resume();
process.stdin.on('data', function(buffer) { jobsString += buffer; });

process.stdin.on('end', function() {
    var renderedEquations = {'DisplayMath' : {}, 'InlineMath' : {}};
    var jobs = JSON.parse(jobsString);

    var i = 0;
    var len = jobs.length;
    for (i = 0; i < len; i++) {
        var type = jobs[i][0]["t"];
        var tex = jobs[i][1];

        var options = {};
        var display = false;
        switch (type) {
            case 'DisplayMath':
                display = true;
                break;
            case 'InlineMath':
                display = false;
                break;
            default:
                console.warn(name +
                             ': Could not understand display option ' +
                             type +
                             ', rendering inline math...');
        }
        options.displayMode = display;
        options.throwOnError = true;

        try {
            renderedEquations[type][tex] = katex.renderToString(tex, options);
        }
        catch (e) {
            exitCode += 1;
            console.error(name + ':\n' + e);
            options.throwOnError = false;
            console.warn('Running KaTeX with "throwOnError = false" ' +
                         'after a failed attempt...');
            renderedEquations[type][tex] = katex.renderToString(tex, options);
        }
    }

    console.log(JSON.stringify(renderedEquations));
    process.exit(exitCode)
});
